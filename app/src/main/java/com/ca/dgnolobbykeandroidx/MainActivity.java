package com.ca.dgnolobbykeandroidx;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.ca.dg.activity.DGDataListener;
import com.ca.dg.activity.DGLoadUtil;
import com.ca.dg.app.AppVariable;
import com.ca.dg.constant.GameId;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends Activity implements View.OnClickListener {

    private Button button1, button2, button3, button4, button5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button1 = (Button) findViewById(R.id.button1);
        button1.setOnClickListener(this);
        button2 = (Button) findViewById(R.id.button2);
        button2.setOnClickListener(this);
        button3 = (Button) findViewById(R.id.button3);
        button3.setOnClickListener(this);
        button4 = (Button) findViewById(R.id.button4);
        button4.setOnClickListener(this);
        button5 = (Button) findViewById(R.id.button5);
        button5.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        //1注意： 请拦截同一时间 多次点击的问题。

        DGLoadUtil.setBarrageHide();// 设置弹幕与聊天隐藏与否， 默认 不隐藏

        //获取URL的信息
//        final String config = "{  " +
//                "\"login\": \"https://www.20266666.com/\", " +
//                "\"bgmVersion\": \"1\"}";

        AppVariable.language = AppVariable.CN;// 要修改语言，在进入游戏之前修改
        switch (v.getId()) {
            case R.id.button1:
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final String jsonString = HttpUtil.sendPost("https://www.20266666.com/game/h5/", "");
                        try {
                            JSONObject myJsonObject = new JSONObject(jsonString);
                            Log.i("我的信息", "myJsonObject" + myJsonObject);
                            final String token = myJsonObject.get("token").toString();
                            Log.i("我的信息", "token" + token);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    /*
                                     * context 当前activity的contenxt
                                     * token DG游戏token
                                     *  gameid: 现在是int类型，要注意传入的值，百家乐 1 龙虎3 轮盘4 骰宝5 牛牛7  如果不是龙虎和牛牛 骰宝 轮盘那么显示百家乐
                                     * config : 配置信息 json字符串
                                     * listener  DG游戏的回调
                                     * minBet 金额的最低值
                                     * */
                                    DGLoadUtil.loadDGGame(MainActivity.this, token, GameId.BACCARAT, getDomains(), new DGDataListener() {
                                        @Override
                                        public void onLoadFinish() {

                                        }

                                        // 数据加载失败的回调，i 有三个类型
                                        // SocketNetError = 0;    socket链接失败
                                        // MemberInitError = 1;   会员初始化失败
                                        // MemberStop = 2;        会员账号暂停
                                        @Override
                                        public void onLoadError(int i) {

                                        }

                                        // 进入DG Activity的回调
                                        @Override
                                        public void onEnter() {

                                        }

                                        // 退出SDK的回调
                                        @Override
                                        public void onExit() {

                                        }
                                    });
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
                break;
            case R.id.button2:
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final String jsonString = HttpUtil.sendPost("https://www.20266666.com/game/h5/", "");
                        try {
                            JSONObject myJsonObject = new JSONObject(jsonString);
                            Log.i("我的信息", "myJsonObject" + myJsonObject);
                            final String token = myJsonObject.get("token").toString();
                            Log.i("我的信息", "token" + token);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    /*
                                     * context 当前activity的contenxt
                                     * token DG游戏token
                                     *  gameid: 现在是int类型，要注意传入的值，百家乐 1 龙虎3 轮盘4 骰宝5 牛牛7  如果不是龙虎和牛牛 骰宝 轮盘那么显示百家乐
                                     * config : 配置信息 json字符串
                                     * listener  DG游戏的回调
                                     * minBet 金额的最低值
                                     * */
                                    DGLoadUtil.loadDGGame(MainActivity.this, token, GameId.DRAGON, getDomains(), new DGDataListener() {
                                        @Override
                                        public void onLoadFinish() {

                                        }

                                        // 数据加载失败的回调，i 有三个类型
                                        // SocketNetError = 0;    socket链接失败
                                        // MemberInitError = 1;   会员初始化失败
                                        // MemberStop = 2;        会员账号暂停
                                        @Override
                                        public void onLoadError(int i) {

                                        }

                                        // 进入DG Activity的回调
                                        @Override
                                        public void onEnter() {

                                        }

                                        // 退出SDK的回调
                                        @Override
                                        public void onExit() {

                                        }
                                    });
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
                break;
            case R.id.button3:
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final String jsonString = HttpUtil.sendPost("https://www.20266666.com/game/h5/", "");
                        try {
                            JSONObject myJsonObject = new JSONObject(jsonString);
                            Log.i("我的信息", "myJsonObject" + myJsonObject);
                            final String token = myJsonObject.get("token").toString();
                            Log.i("我的信息", "token" + token);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    /*
                                     * context 当前activity的contenxt
                                     * token DG游戏token
                                     *  gameid: 现在是int类型，要注意传入的值，百家乐 1 龙虎3 轮盘4 骰宝5 牛牛7  如果不是龙虎和牛牛 骰宝 轮盘那么显示百家乐
                                     * config : 配置信息 json字符串
                                     * listener  DG游戏的回调
                                     * minBet 金额的最低值
                                     * */
                                    DGLoadUtil.loadDGGame(MainActivity.this, token, GameId.BULL, getDomains(), new DGDataListener() {
                                        @Override
                                        public void onLoadFinish() {

                                        }

                                        // 数据加载失败的回调，i 有三个类型
                                        // SocketNetError = 0;    socket链接失败
                                        // MemberInitError = 1;   会员初始化失败
                                        // MemberStop = 2;        会员账号暂停
                                        @Override
                                        public void onLoadError(int i) {

                                        }

                                        // 进入DG Activity的回调
                                        @Override
                                        public void onEnter() {

                                        }

                                        // 退出SDK的回调
                                        @Override
                                        public void onExit() {

                                        }
                                    });
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
                break;
            case R.id.button4:
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final String jsonString = HttpUtil.sendPost("https://www.20266666.com/game/h5/", "");
                        try {
                            JSONObject myJsonObject = new JSONObject(jsonString);
                            Log.i("我的信息", "myJsonObject" + myJsonObject);
                            final String token = myJsonObject.get("token").toString();
                            Log.i("我的信息", "token" + token);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    /*
                                     * context 当前activity的contenxt
                                     * token DG游戏token
                                     *  gameid: 现在是int类型，要注意传入的值，百家乐 1 龙虎3 轮盘4 骰宝5 牛牛7  如果不是龙虎和牛牛 骰宝 轮盘那么显示百家乐
                                     * config : 配置信息 json字符串
                                     * listener  DG游戏的回调
                                     * minBet 金额的最低值
                                     * */
                                    DGLoadUtil.loadDGGame(MainActivity.this, token, GameId.ROULETTE, getDomains(), new DGDataListener() {
                                        @Override
                                        public void onLoadFinish() {

                                        }

                                        // 数据加载失败的回调，i 有三个类型
                                        // SocketNetError = 0;    socket链接失败
                                        // MemberInitError = 1;   会员初始化失败
                                        // MemberStop = 2;        会员账号暂停
                                        @Override
                                        public void onLoadError(int i) {

                                        }

                                        // 进入DG Activity的回调
                                        @Override
                                        public void onEnter() {

                                        }

                                        // 退出SDK的回调
                                        @Override
                                        public void onExit() {

                                        }
                                    });
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
                break;
            case R.id.button5:
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final String jsonString = HttpUtil.sendPost("https://www.20266666.com/game/h5/", "");
                        try {
                            JSONObject myJsonObject = new JSONObject(jsonString);
                            Log.i("我的信息", "myJsonObject" + myJsonObject);
                            final String token = myJsonObject.get("token").toString();
                            Log.i("我的信息", "token" + token);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    /*
                                     * context 当前activity的contenxt
                                     * token DG游戏token
                                     *  gameid: 现在是int类型，要注意传入的值，百家乐 1 龙虎3 轮盘4 骰宝5 牛牛7  如果不是龙虎和牛牛 骰宝 轮盘那么显示百家乐
                                     * config : 配置信息 json字符串
                                     * listener  DG游戏的回调
                                     * minBet 金额的最低值
                                     * */
                                    DGLoadUtil.loadDGGame(MainActivity.this, token, GameId.SICBO, getDomains(), new DGDataListener() {
                                        @Override
                                        public void onLoadFinish() {

                                        }

                                        // 数据加载失败的回调，i 有三个类型
                                        // SocketNetError = 0;    socket链接失败
                                        // MemberInitError = 1;   会员初始化失败
                                        // MemberStop = 2;        会员账号暂停
                                        @Override
                                        public void onLoadError(int i) {

                                        }

                                        // 进入DG Activity的回调
                                        @Override
                                        public void onEnter() {

                                        }

                                        // 退出SDK的回调
                                        @Override
                                        public void onExit() {

                                        }
                                    });
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
                break;
        }
    }


    /**
     * 在登录的时候 会返回这个配置信息 domains
     * @return
     */
    private String getDomains() {
        String domains1 = "{\"file\": \"https://dg-asia.taxyss.com/\",\"tcp\": \"appa.fuqiangtools.com:8860\", " +
                "\"game_wss\": \"wss://appa.fuqiangtools.com:8871\", " +
                "\"freeToken\":\"https://login.dg0.co/game/h5/\", " +
                "\"login\": \"https://login.dg0.co/\",\"chat_wss\": " +
                "\"wss://appa.fuqiangtools.com:8873\",\"back_img\": " +
                "\"https://tupian-dg.dg0.co/#https://tupian-ct.dg0.co/#https://tupian.nanhai666.com/image/\", " +
                "\"back_video\": \"https://videoback-dg.dg0.co/#https://videoback-ct.dg0.co/#https://playback.nanhai66.com/\", " +
                "\"back_appvideo\": \"rtmp://play-ct.dg0.co/vod/mp4#rtmp://play-ct.dg0.co:1936/vod/mp4#rtmp://playback.nanhai66.com/vod/mp4\", " +
                "\"dealerFile\": \"https://dg-asia.taxyss.com/#https://ct-asia.aogebgjj.com/#https://nh-asia.17xiangqu.com/\", " +
                "\"bgmVersion\": \"2\"\n}";
        return domains1;
    }
}
