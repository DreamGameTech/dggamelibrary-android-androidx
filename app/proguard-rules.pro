# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in D:\tools\eclipse\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html
#
# Add any project specific keep options here:
-optimizationpasses 5   #指定执行几次优化 默认执行一次
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontpreverify
-ignorewarnings
-allowaccessmodification
-verbose
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*

-keep class androidx.** { *; }
-keep interface androidx.** { *; }
#androidx.
-keep public class * extends android.app.Application


-keep class com.ca.dg.activity.DGLoadUtil{ *; }
-keep interface com.ca.dg.activity.DGDataListener{ *; }
-keep class com.ca.dg.constant.GameId{ *; }
-keep public class * extends android.app.Activity{ public *;}

-keepattributes LineNumberTable,SourceFile
-keep class com.bugtags.library.** {*;}
-dontwarn org.apache.http.**
-dontwarn android.net.http.AndroidHttpClient
-dontwarn com.bugtags.library.vender.**
-dontwarn com.bugtags.library.**

-keep class com.ca.dg.biz.RoadBiz{*;}

#-keep public class * extends android.view.View {
#    public <init>(android.content.Context);
#    public <init>(android.content.Context, android.util.AttributeSet);
#    public <init>(android.content.Context, android.util.AttributeSet, int);
#    public void set*(...);
#    public void get*(...);
#    public static *(...);
#    public *(...);
#}
-keep public class * extends android.view.View {*;}

-keep class cn.nodemedia.** { *; }


-keepclasseswithmembernames class * {     # 保持 native 方法不被混淆
    native <methods>;
}
-keep public class * implements android.os.Parcelable

-keepclassmembers class * implements java.io.Serializable { *; }

-keep class androidx.** {*;}
-keep public class * extends androidx.**
-keep interface androidx.** {*;}
-dontwarn androidx.**

-keep public class com.ca.dg.R$*{
    public static final int *;
}
-keepclassmembers class **.R$* {
 public static <fields>;
}

-keepattributes *Annotation*
-keepattributes Signature,InnerClasses
-keepclasseswithmembers class io.netty.** { *; }
-keepnames class io.netty.** { *; }

-keep class com.google.protobuf.** { *; }
-keep interface com.google.protobuf.** { *; }

-dontwarn com.alibaba.fastjson.**
-keep class com.alibaba.fastjson.** { *; }
-keep interface com.alibaba.fastjson.** { *; }

-keep class com.pili.pldroid.** { *; }
-keep interface com.pili.pldroid.** { *; }
-keep class tv.danmaku.** { *; }

-keepclassmembers public class fqcn.of.javascript.interface.for.webview {
   public *;
}

-keep public class com.ca.dg.model.** { *; }
-keep public class com.ca.dg.viewModel.** { *; }
-keep public class com.ca.dg.view.custom.bet.ImgsLocal { *; }
-keep public class com.ca.dg.util.** { *; }

-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}

-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keep class androidx.databinding.** { *; }
-keep class * extends androidx.databinding.** { *; }
-keep class androidx.databinding.** {*;}

-keep class me.weishu.reflection.** { *; }

-keep class android.databinding.** { *; }
-keep class com.ca.dg.databinding.** { *; }

-dontwarn android.databinding.**

-keep class com.ca.dg.app.AppVariable{*;}
-keep class com.github.bumptech.glide.** {*;}

-keepnames class * implements java.io.Serializable
-keepattributes javax.xml.bind.annotation.*
-keepattributes javax.annotation.processing.*

-keepclassmembers class * extends java.lang.Enum { *; }

-keepclasseswithmembernames class android.**
-keepclasseswithmembernames class androidx.**

-keep public class * extends android.view.DataBinderMapper {*;}
-keep public class * extends androidx.view.DataBinderMapper {*;}
-keep public class * extends android.app.Fragment {*;}

#com.ca.dg.
-keep public class **.BR { public *; }
-keep public class **.BR$* { public *; }
-keepclassmembers class **.BR$* {
    public static <fields>;
}