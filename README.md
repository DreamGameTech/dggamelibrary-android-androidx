## DGGameLibrary 是DreamGame Technology 公司游戏产品的SDK，供平台商对接我司游戏使用

## 该SDK包含DG 百家乐、龙虎、牛牛、轮盘、骰宝游戏

### 对接方式

### 文档修改日志

版本号 | 修改内容 | 修改日期
----|------|----
10.0 | 1.登录接口添加配置文件传入，对接登录API，返回配置文件字符串，传入SDK。  2.添加轮盘、骰宝游戏。   3.SDK大小优化，减少11M。    | 2019-10-23
10.1 | 1.视频链接加密。    | 2019-10-23

#### 1. 引入DG SDK
(1) 手动导入：
            将示例项目中的 dggamelibrary.arr文件复制到项目中的libs下,并在app的build.gradle的根节点下添加
```
repositories{
    flatDir{
        dirs 'libs'
    }
}    

```
在dependencies节点下添加添加
```
implementation(name:'dggamelibrary', ext:'aar')
```
在android 节点下添加
```
dataBinding {
    enabled = true
}
```
在defaultConfig 节点下添加
```
ndk {
    abiFilters "armeabi"
}
```
 


(2) 自动构建：（该库不支持）

#### 2. 添加相应依赖
 为避免添加依赖库的冲突，所以提出来一些常用的公共库，在外部app中添加依赖，需要在app的build.gradle中添加如下依赖
```
    implementation 'androidx.appcompat:appcompat:1.0.0'
    implementation 'androidx.recyclerview:recyclerview:1.0.0'
    implementation 'androidx.legacy:legacy-support-v4:1.0.0'
    implementation 'androidx.annotation:annotation:1.0.0'
    implementation 'com.github.bumptech.glide:glide:4.9.0'
    annotationProcessor 'com.github.bumptech.glide:compiler:4.9.0'
```
#### 2. 添加权限
```
   <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE"/>
   <uses-permission android:name="android.permission.ACCESS_WIFI_STATE"/>
   <uses-permission android:name="android.permission.INTERNET"/>
   <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
   <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
   <uses-permission android:name="android.permission.READ_SYNC_SETTINGS"/>
   <uses-permission android:name="android.permission.READ_PHONE_STATE"/>
```

#### 3. 传入token并跳转进去DG 厅
```
例如
/*
 * context      当前activity的contenxt
 * token        DG游戏token
 * GamdId       现在是int类型，要注意传入的值，目前只支持 百家乐 龙虎 牛牛 轮盘 骰宝
 * domains   获取配置文件的字符串， 对接登录api会提供
 * listener     DG游戏的回调
 * minBet       传入游戏最低下注额(可不传)
 */
DGLoadUtil.loadDGGame(MainActivityWaike.this, token, GameId.BACCARAT, domains, new DGDataListener() {
    // 数据加载完成的回调
    @Override
    public void onLoadFinish() {

    }
    // 数据加载失败的回调，i 有三个类型
    // SocketNetError = 0;    socket链接失败
    // MemberInitError = 1;   会员初始化失败
    // MemberStop = 2;        会员账号暂停
    @Override
    public void onLoadError(int i) {

    }
    // 进入DG Activity的回调
    @Override
    public void onEnter() {

    }
    // 退出SDK的回调
    @Override
    public void onExit() {

    }
},minBet);
```